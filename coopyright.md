_Version 1.1 du 26/12/2017_

Préambule :

La présente politique vise à fixer les principes de gouvernance en vigueur au sein de l’association « La Coop des Communs » pour la gestion des droits sur les productions de ses membres, notamment dans le cadre de l’activité de ses groupes de travail.

Cette politique poursuit plusieurs objectifs, qui doivent servir à guider l’interprétation des dispositions de ce document :

- la volonté de diffuser le plus largement possible les productions de La Coop des Communs ;
- la reconnaissance des contributions que les membres apportent à La Coop des Communs ;
- un esprit de réciprocité, aussi bien entre les membres de La Coop des Communs qu’envers les personnes et entités extérieures ;
- l’intention de considérer les productions de l’association comme un Commun géré collectivement ;
- le souci de garantir la soutenabilité des organisations non lucratives ou à lucrativité limitée et, notamment, celle des membres de La Coop des Communs et les siennes propres.

Cette politique est partie intégrante du règlement intérieur de l’association. Elle peut être modifiée sur proposition du Conseil d’animation de l’association, après approbation par l’Assemblée Générale des membres.

Le fait de devenir membre de l’association La Coop des Communs vaut approbation de ces principes.

Le Conseil d’animation de l’association est garant de l’application et de la bonne observation de cette politique.

**1.    Statut des productions des groupes de travail**

Par productions des groupes de travail, on entend tous les contenus élaborés dans ce cadre et susceptibles d’être protégés par un droit de propriété intellectuelle, quelle que ce soit leur nature : textes, modèles, documents-type, outils de gestion, etc. (hormis les logiciels, voir point 9).

Chaque groupe de travail détermine librement en son sein, selon les modalités de décision qu’il aura adoptées, si ses productions doivent être publiées et la manière dont elles doivent être portées à la connaissance du public.

Chaque groupe de travail choisit de présenter ses productions :

- soit comme des œuvres individuelles (signées par un seul auteur) ;
- soit comme des œuvres de collaboration (émanant de plusieurs co-auteurs identifiés) ;
- soit comme des œuvres collectives (attribuées au groupe lui-même en tant qu’entité).

**2.    Licence par défaut des productions des groupes de travail**

Par défaut et au cas où elles font l’objet d’une publication, les productions des groupes de travail sont placées sous la licence Creative Commons CC-BY-NC-ND 4.0 (Attribution – Pas d’Usage Commercial – Pas de modification) [1].

Cette licence vaut lorsque la réutilisation des productions est le fait de personnes ou d’entités n’ayant pas la qualité de « contributeurs actifs à La Coop des Communs » (voir point 7).

Par exception, les groupes de travail peuvent demander à placer leurs productions sous une autre licence que la CC-BY-NC-ND 4.0, à condition que des circonstances particulières le justifient et après approbation du Conseil d’animation de l’association La Coop des Communs.

**3.    Attribution des productions des groupes de travail**

Il est procédé à l’Attribution des productions (respect de la condition de Paternité) en fonction de leur statut, tel que décidé par les groupes de travail (voir point 1) :

- pour les œuvres individuelles ou de collaboration : mention du nom du ou des auteurs ;
- pour les œuvres collectives : mention du nom du groupe de travail.

Dans tous les cas, l’indication du nom du ou des auteurs est suivie de la mention : Document produit dans le cadre des travaux de l’association « La Coop des Communs » et, dans la mesure du possible, du logo de La Coop des Communs.

**4.    Réutilisation des productions des groupes de travail impliquant des modifications**

Selon les termes de la licence CC-BY-NC-ND 4.0, la modification des productions des groupes de travail – à savoir toutes les formes d’adaptation et de réalisation d’œuvres dérivées, y compris les traductions – implique la délivrance d’une autorisation préalable.

Pour les œuvres individuelles et les œuvres de collaboration, cette autorisation est accordée par les membres identifiés par les groupes de travail comme auteur ou co-auteurs des productions.

Pour les œuvres collectives, cette autorisation est accordée par le groupe, selon les modalités de décision qu’il aura choisies d’adopter.

**5.    Réutilisation des productions des groupes de travail impliquant une exploitation commerciale**

Selon les termes de la licence CC-BY-NC-ND 4.0, l’usage commercial des productions des groupes de travail implique la délivrance d’une autorisation préalable.

Pour les œuvres individuelles et les œuvres de collaboration, cette autorisation est accordée par les membres identifiés par les groupes comme auteur ou co-auteurs des productions.

Pour les œuvres collectives, cette autorisation est accordée par le groupe, selon les modalités de décision qu’il aura choisies d’adopter.

En cas d’usage commercial, l’association pourra, en accord avec le ou les auteurs du contenu, exiger le versement d’une redevance de la part des réutilisateurs.

Une grille de tarifs pourra être adoptée à cette fin par l’association. Pour tous les cas non prévus par cette grille, le montant des redevances est établi par le Conseil d’animation de l’association, en concertation avec les membres du groupe de travail concerné par l’usage.

Les sommes perçues au titre de ces redevances de réutilisation sont versées au budget de l’association et sont utilisées pour financer ses activités.

**6.    Exonération de redevances en cas de réutilisation par une entité à but non-lucratif ou à lucrativité limitée**

Sont dispensées d’autorisation préalable et exonérées du paiement de redevances, telles que prévues au point 5, les réutilisations commerciales opérées par des personnes ou des entités oeuvrant dans le cadre d’une activité à but non-lucratif ou à lucrativité limitée. La réutilisation s’effectue alors selon les termes de la licence Creative Commons CC-BY-SA 4.0 (Paternité – Partage à l’identique).

L’appréciation du but non-lucratif ou de la lucrativité limitée de l’entité s’opère à partir des critères fixés par le droit national dont relève le réutilisateur.

Les entités à but non-lucratif ou à lucrativité limitée réutilisant des productions de La Coop des Communs sont invités à informer l’association en cas de réutilisation.

**7.      Réutilisation des productions par des « contributeurs actifs à La Coop des Communs »**

La qualité de « contributeur actif à la Coop des Communs » s’acquiert par le cumul des deux conditions suivantes :

- être membre de l’association « La Coop des Communs » et être à jour de ses cotisations ;  
- participer activement à un ou plusieurs groupes de travail de l’association.

Les groupes de travail dressent chaque année la liste de leurs membres actifs et la transmettent au Conseil d’animation, de manière à pouvoir établir l’identité des contributeurs actifs à La Coop des Communs.

Pour les membres ayant la qualité de contributeur actif à La Coop des Communs, la réutilisation des productions des groupes de travail s’effectue selon les termes de la licence Creative Commons CC-BY-SA 4.0 (Paternité – Partage à l’identique) [2].

En cas d'usage impliquant une republication, les membres ayant qualité de contributeur actif à la Coop des Communs s'engagent à utiliser les licences suivantes :

- si la production de la Coop des Communs est republiée à l'identique, sans modification, elle devra être placée sous licence CC-BY-NC-ND 4.0 ;
- si une oeuvre dérivée est produite à partir d'une production de la Coop des Communs et publiée, elle devra être placée sous licence CC-BY-SA 4.0.

**8.    Productions de La Coop des Communs n’émanant pas des groupes de travail**

Les productions de La Coop des Communs n’émanant pas de ses groupes de travail sont soumises, dans la mesure du possible, aux principes de cette politique de réutilisation, et ce quelle que soit leur nature.

Lorsque des productions de La Coop des Communs font à la fois l’objet de droits appartenant directement à l’association (droits voisins sur des enregistrements notamment) et de droits appartenant à ses membres, elles sont placées par défaut sous licence CC-BY-ND 4.0 vis-à-vis des tiers et peuvent être réutilisées sous licence CC-BY-SA 4.0 par les contributeurs actifs.

Lorsque des contenus sont issus de personnes extérieures à l’association, mais produites dans le cadre d’événements ou d’activités organisés par ses soins, La Coop des Communs produira le meilleur effort pour obtenir de ces personnes extérieures les autorisations nécessaires afin que les contenus puissent être diffusés en accord avec les principes de cette politique.

**9.    Cas particulier des logiciels**

Les licences Creative Commons ne sont pas adaptées à la diffusion des logiciels.

Chaque groupe de travail choisit donc en conséquence la ou les licences sous lesquelles il souhaite diffuser les logiciels produits dans le cadre de son activité, en concertation avec le CA de l’association.

Ces licences sont sélectionnées en cohérence avec les objectifs de la présente politique, tels qu’énoncés dans son préambule.

En cas de besoin, les mêmes principes sont appliqués pour tout type de productions pour lesquelles les licences Creative Commons ne seraient pas appropriées.

**10.  Application de la présente politique de réutilisation**

Les membres de l’association s’engagent à respecter et à appliquer cette politique de réutilisation dans un esprit de bonne entente et de compréhension mutuelle, en produisant les meilleurs efforts pour aboutir à un consensus.

Au cas où un désaccord surviendrait au sein d’un groupe de travail à propos de l’application de cette politique, les membres conviennent d’en saisir le CA de l’association, qui proposera une solution dans un esprit de conciliation, en associant au besoin les autres membres de l’association.

[1] https://creativecommons.org/licenses/by-nc-nd/4.0/deed.fr  
[2] https://creativecommons.org/licenses/by-sa/4.0/deed.fr